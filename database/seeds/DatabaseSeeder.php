<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(EstatusTableSeeder::class);
        $this->call(TipoUsuarioTableSeeder::class);
		$this->call(UserTableSeeder::class);
        $this->call(EspecieTableSeeder::class);
        $this->call(AnimalTableSeeder::class);    
        $this->call(TipoNoticiaTableSeeder::class);
        $this->call(NoticiaTableSeeder::class);
        $this->call(EstatusDonacionTableSeeder::class);
        $this->call(DonacionTableSeeder::class);
    }
}
