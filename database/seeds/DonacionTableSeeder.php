<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DonacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker= Faker::create();


        $estatus_activo= DB::table('estatus')
                                        ->select('id')
                                        ->where('nombre', 'activo')
                                        ->first()
                                        ->id;


    	for($i=0; $i<50; $i++){

            \DB::table('donacion')->insert(array(
        	'idusuario' => $faker->numberBetween(2, 8) ,
        	'fecha' => $estatus_activo,
        	'idestatusdonacion'=>$faker->numberBetween(1, 3) ,
        	'cantidad' =>$faker->numberBetween(50, 10000),
        	'referencia'=>$faker->numberBetween(1000,9999) ,
        	));	
        }
    }
}
