<?php

use Illuminate\Database\Seeder;

class EstatusDonacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         \DB::table('estatusdonacion')->insert(array(
        	'nombre' => 'Confrimada'
        	));
        \DB::table('estatusdonacion')->insert(array(
        	'nombre' => 'Negada'
        	));	

         \DB::table('estatusdonacion')->insert(array(
        	'nombre' => 'En Espera'
        	));	
    }
}
