<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idusuario')->unsigned();
            $table->integer('idestatusdonacion')->unsigned();
            $table->text('fecha');
            $table->mediumText('referencia');
            $table->integer('cantidad');
            $table->timestamps();

             $table->foreign('idusuario')
                        ->references('id')
                        ->on('usuario')
                        ->onDelete('cascade');


            $table->foreign('idestatusdonacion')
                        ->references('id')
                        ->on('estatusdonacion')
                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('donacion');
    }
}
