<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idtipousuario')->unsigned();
            $table->integer('idestatususuario')->unsigned();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('correo')->unique();
            $table->string('telefono');
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();


            $table->foreign('idtipousuario')
                        ->references('id')
                        ->on('tipousuario')
                        ->onDelete('cascade');


            $table->foreign('idestatususuario')
                        ->references('id')
                        ->default(1)
                        ->on('estatus')
                        ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('noticia');
        //Schema::drop('donacion');
        Schema::drop('usuario');
    }
}
