<?php

namespace App;

use Validator;

use Illuminate\Database\Eloquent\Model;

class Especie extends Model
{
        protected $table = 'especie';

        protected $fillable = ['nombre'];


     public $errors;

  

   

    public static $messages;

       public  static function isValid($data)
    {
        $rules = array(
            'nombre'     => 'required|unique:especie'
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }


     	
   
}
