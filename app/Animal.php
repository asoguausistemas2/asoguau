<?php

namespace App;
use Validator;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
 
  protected $table = 'animal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idespecie','idestatususuario','nombre','comentario'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
     public $errors;
    

    public static $messages;

    public static function isValid($data)
    {
        $rules = array(
            'idespecie'   => 'required',
        'idestatususuario' => 'required',
        'nombre' => 'required|unique:animal'
            
            //'' => 'required|min:4|max:40',
            //'password'  => 'required|min:8|confirmed',
            //'password_confirmation' => 'required|min:8|same:password',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $errors = $validator->errors();
        
        return false;
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function especie()
    {
        return $this->belongsTo('App\Especie', 'idespecie', 'id');
    }

    public function estatus()
    {
        return $this->belongsTo('App\EstatusUsuario', 'idestatususuario', 'id');
    }
}
