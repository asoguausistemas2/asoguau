<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Validator;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idtipousuario','idestatususuario','nombre','apellido','telefono','correo', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token','idestatususuario'];

     public $errors;
    


    public static $messages;

    public static function  isValid($data)
    {
        $rules = array(
            'nombre'   => 'required',
        'apellido' => 'required',
        'telefono' => 'required',
       // 'correo'   => 'required|email|unique:usuario',
        'password' => 'required'
            //'' => 'required|min:4|max:40',
            //'password'  => 'required|min:8|confirmed',
            //'password_confirmation' => 'required|min:8|same:password',
        );
        
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
            $errors = $validator->errors();
        
        return false;
    }



    public function isValid2($data)
    {
        $rules = array(
            'correo'     => 'required|email',
           // 'nombre' => 'required|min:4|max:40',
           // 'apellido' => 'required|min:4|max:40',
           // 'telefono' => 'required|min:4|max:40',
            'password'  => 'required',
            //'password_confirmation' => 'required|min:8|same:password',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function rol()
    {
        return $this->belongsTo('App\Rol', 'idtipousuario', 'id');
    }
}