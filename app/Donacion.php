<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donacion extends Model
{
     protected $table = 'donacion';

    protected $fillable = ['idusuario','idestatusdonacion','fecha','referencia','cantidad'];


     public $errors;

  

   

    public static $messages;

       public  static function isValid($data)
    {
        $rules = array(
          //  'idusuario'     => 'required',
           //'idestatusdonacion' => 'required',
            //'referencia' => 'required',
            //'cantidad'=>'required'
            //'' => 'required|min:4|max:40',
            //'password'  => 'required|min:8|confirmed',
            //'password_confirmation' => 'required|min:8|same:password',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }


     public function usuario()
    {
        return $this->belongsTo('App\User', 'idusuario', 'id');
    }

      public function estatus()
    {
        return $this->belongsTo('App\EstatusDonacion', 'idestatusdonacion', 'id');
    }

}
