<?php

namespace App;
use Validator;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $table = 'noticia';

    protected $fillable = ['idtiponoticia','idusuario','titulo','descripcion','resumen','hora'];


     public $errors;

  

   

    public static $messages;

       public  static function isValidNoticia($data)
    {
        $rules = array(
            'titulo'     => 'required',
            'resumen' => 'required|min:4|max:140',
            'descripcion' => 'required|min:4|max:10000',
            'idusuario' => 'required',
            'idtiponoticia' => 'required',
            
            //'' => 'required|min:4|max:40',
            //'password'  => 'required|min:8|confirmed',
            //'password_confirmation' => 'required|min:8|same:password',
        );
        
        $validator = Validator::make($data, $rules);
        
        if ($validator->passes())
        {
            return true;
        }
        
        $this->errors = $validator->errors();
        
        return false;
    }


     public function rol()
    {
        return $this->belongsTo('App\User', 'idusuario', 'id');
    }
   
}
