<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Animal;
use Redirect;
use Input;

class AnimalController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

        public function index()
    {
        $animales = Animal::paginate(10);

        return view('backend.modules.animales.manage.index', compact('animales'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* Este if de aqui se encadena al metodo de isValid dsde el modelo de User */
        $data = array(
                          'nombre' => Input::get('nombre'),
                          'idespecie' => Input::get('idespecie'),
                          'comentario' => Input::get('comentario'),
                          'idestatususuario' => Input::get('idestatususuario')
                          
                       );
        if(!Animal::isValid(Input::all()))
        {
            return Redirect::back()->withInput()->withErrors(Animal::$messages);
        }

          $animal = new Animal;
          $animal->fill($data);
            // Guardamos el usuario
            $animal->save();


        return Redirect::to('animales.index')->with('succes', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Animal::find($id);
        $animales = Animal::paginate(10);

        return view('backend.modules.animales.manage.index', compact('edit', 'animales'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Animal::find($id)->update(Input::all());

        return Redirect::route('animales.index')->with('success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $animal = Animal::find($id);
        $animal->delete();
        /**
         * HardDeletes
         *
         * User::find($id)->delete();
         */
        return Redirect::route('animales.index')->with('delete', true);
    }

    public function backend()
    {
        return view('backend.index');
    }

    public function inactivos()
    {
        $inactivos = Noticia::onlyTrashed()->paginate();

        return view('backend.modules.animales.status.inactivos', compact('inactivos'));
    }

    public function activos($id)
    {
        Noticia::withTrashed()->find($id)->restore();

        return Redirect::route('animales.index');
    }
}
