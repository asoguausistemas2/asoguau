<?php
namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use View;
use Redirect;
use Input;
use App\User;
use Hash;
use App\Noticia;
use App\Donacion;


class AuthController extends Controller
{


    public function index(){
        // Recordar Importar el Modelo al usar
        $noticias=Noticia::all(); //Variable apuntando al modelo y traerme todos los datos de la base de datos
        
        //return dd($usuarios);
        return view('home')->with('noticias',$noticias); //metodo compact para compactar todas las variables
    }


    public function miasoguauindex(){
        // Recordar Importar el Modelo al usar
        $noticias=Noticia::all(); //Variable apuntando al modelo y traerme todos los datos de la base de datos
        
        //return dd($usuarios);
        
        $donaciones = Donacion::all();

        return view('auth/miasoguau', compact('noticias', 'donaciones'));
       // return view('home')->with('noticias',$noticias); //metodo compact para compactar todas las variables
    }



   public function showLogin()
    {
        // Verificamos si hay sesión activa
        if (Auth::check())
        {
            // Si tenemos sesión activa mostrará la página de inicio
            return Redirect::to('/');
        }
        // Si no hay sesión activa mostramos el formulario
        return View::make('auth/login');
    }

    public function postLogin()
    {
        // Obtenemos los datos del formulario
        $data = [
            'correo' => Input::get('correo'),
            'password' => Input::get('password')
        ];
 
        // Verificamos los datos
        if (Auth::attempt($data,Input::get('remember'))) // Como segundo parámetro pasámos el checkbox para sabes si queremos recordar la contraseña
        {
            // Si nuestros datos son correctos mostramos la página de inicio
            //return Redirect::intended('/');
            return Redirect::to('/');
        }
        // Si los datos no son los correctos volvemos al login y mostramos un error
        return Redirect::back()->with('error_message', 'Invalid data')->withInput();
    }

    public function logOut()
    {
        // Cerramos la sesión
        Auth::logout();
        // Volvemos al login y mostramos un mensaje indicando que se cerró la sesión
        return Redirect::to('login')->with('error_message', 'Logged out correctly');
    }



      public function store()
    {
        // Creamos un nuevo objeto para nuestro nuevo usuario
        $user = new User;
        // Obtenemos la data enviada por el usuario
        $password = Hash::make(Input::get('password'));
        $password_confirmation=Hash::make(Input::get('password_confirmation'));
        $data = array(
                          'nombre' => Input::get('nombre'),
                          'apellido' => Input::get('apellido'),
                          'telefono' => Input::get('telefono'),
                          'correo' => Input::get('correo'),
                          'idestatususuario' => Input::get('idestatususuario'),
                          'idtipousuario' => Input::get('idtipousuario'),
                          'password' => $password,
                        //  'password_confirmation' => $password_confirmation,
                       );
        
        // Revisamos si la data es válido
        if ($user->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $user->fill($data);
            // Guardamos el usuario
            $user->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario

            $data= Input::only('correo','password');
            if (Auth::attempt($data)) // Como segundo parámetro pasámos el checkbox para sabes si queremos recordar la contraseña
                {
                    // Si nuestros datos son correctos mostramos la página de inicio
                    //return Redirect::intended('/');
                    return Redirect::to('/');
                }
        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
return Redirect::to('registro')->withInput()->withErrors($user->errors);
        }
    }




    public function vernoticia(){
        // Recordar Importar el Modelo al usar
        $noticias=Noticia::all(); //Variable apuntando al modelo y traerme todos los datos de la base de datos
        
        //return dd($usuarios);
        return view('auth/noticia')->with('noticias',$noticias); //metodo compact para compactar todas las variables
    }




    public function crearAsored()
    {
        // Creamos un nuevo objeto para nuestro nuevo usuario
        $noticia = new Noticia;
        // Obtenemos la data enviada por el usuario
        
        $data = array(
                          'titulo' => Input::get('titulo'),
                          'resumen' => Input::get('resumen'),
                          'descripcion' => Input::get('descripcion'),
                          'hora' => Input::get('hora'),
                          'idtiponoticia' => Input::get('idtiponoticia'),
                          'idusuario' => Input::get('idusuario')
                         
                       );
        
        // Revisamos si la data es válido
       if ($noticia->isValidNoticia($data))
        {
            // Si la data es valida se la asignamos al usuario
            $noticia->fill($data);
            // Guardamos el usuario
            $noticia->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario

                    return Redirect::to('miasoguau');

        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
            return Redirect::to('noticiaAsored')->withInput()->withErrors($noticia->errors);;
        }
    }
}
