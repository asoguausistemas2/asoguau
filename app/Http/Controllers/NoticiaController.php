<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Noticia;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class NoticiaController extends Controller
{
    	public function index()
	{
		$noticias = Noticia::paginate(10);

		return view('backend.modules.noticias.manage.index', compact('noticias'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		/* Este if de aqui se encadena al metodo de isValid dsde el modelo de User */

		if(!Noticia::isValidNoticia(Input::all()))
		{
			return Redirect::back()->withInput()->withErrors(Noticia::$messages);
		}

		Noticia::create(Input::all());

		return Redirect::route('noticiasbackend.index')->with('succes', true);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$edit = Noticia::find($id);
		$noticias = Noticia::paginate(10);

		return view('backend.modules.noticias.manage.index', compact('edit', 'noticias'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		Noticia::find($id)->update(Input::all());

		return Redirect::route('noticiasbackend.index')->with('success', true);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$noticia = Noticia::find($id);
		$noticia->delete();
		/**
		 * HardDeletes
		 *
		 * User::find($id)->delete();
		 */
		return Redirect::route('noticiasbackend.index')->with('delete', true);
	}

	public function backend()
	{
		return view('backend.index');
	}

	public function inactivos()
	{
		$inactivos = Noticia::onlyTrashed()->paginate();

		return view('backend.modules.noticias.status.inactivos', compact('inactivos'));
	}

	public function activos($id)
	{
		Noticia::withTrashed()->find($id)->restore();

		return Redirect::route('noticiasbackend.index');
	}
	
}
