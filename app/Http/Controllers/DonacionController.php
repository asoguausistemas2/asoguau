<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Donacion;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
class DonacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $donaciones = Donacion::paginate(10);

        return view('backend.modules.donaciones.manage.index', compact('donaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $donacion = new Donacion;
        // Obtenemos la data enviada por el usuario
        
        $data = array(
                          'catidad' => Input::get('cantidad'),
                          'referencia' => Input::get('referencia'),
                          'fecha' => Input::get('fecha'),
                          'hora' => Input::get('hora'),
                          'idusauario' => Input::get('idusuario'),
                          'idesatusdonacion' => Input::get('idestatusdonacion')
                         
                       );
        
        // Revisamos si la data es válido
       if ($donacion->isValid($data))
        {
            // Si la data es valida se la asignamos al usuario
            $donacion->fill($data);
            // Guardamos el usuario
            $donacion->save();
            // Y Devolvemos una redirección a la acción show para mostrar el usuario

                    return Redirect::to('miasoguau');

        }
        else
        {
            // En caso de error regresa a la acción create con los datos y los errores encontrados
            return Redirect::to('misasoguau')->withInput()->withErrors($donacion->errors);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function edit($id)
    {
        $edit = Donacion::find($id);
        $donaciones = Donacion::paginate(10);

        return view('backend.modules.donaciones.manage.index', compact('edit', 'donaciones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Donacion::find($id)->update(Input::all());

        return Redirect::route('donacionesbackend.index')->with('success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donaciones = Donacion::find($id);
        $donaciones->delete();
        /**
         * HardDeletes
         *
         * User::find($id)->delete();
         */
        return Redirect::to('miasoguau');
    }


    public function backend()
    {
        return view('backend.index');
    }

    public function inactivos()
    {
        $inactivos = Donacion::onlyTrashed()->paginate();

        return view('backend.modules.donaciones.status.inactivos', compact('inactivos'));
    }

    public function activos($id)
    {
        Donacion::withTrashed()->find($id)->restore();

        return Redirect::route('donacionesbackend.index');
    }
}
