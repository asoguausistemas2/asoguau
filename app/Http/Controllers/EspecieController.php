<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Especie;
use Input;
use App\Animal;
use Redirect;

class EspecieController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

        public function index()
    {
        $especies = Especie::paginate(10);

        return view('backend.modules.especies.manage.index', compact('especies'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* Este if de aqui se encadena al metodo de isValid dsde el modelo de User */
        $data = array(
                          'nombre' => Input::get('nombre'),
                          
                       );
        if(!Especie::isValid(Input::all()))
        {
            return Redirect::back()->withInput()->withErrors(Especie::$messages);
        }

          $especie = new Especie;
          $especie->fill($data);
            // Guardamos el usuario
            $especie->save();


        return Redirect::route('especies.index')->with('succes', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Especie::find($id);
        $especies = Especie::paginate(10);

        return view('backend.modules.especies.manage.index', compact('edit', 'especies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Especie::find($id)->update(Input::all());

        return Redirect::route('especies.index')->with('success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $especie = Especie::find($id);
        $especie->delete();
        /**
         * HardDeletes
         *
         * User::find($id)->delete();
         */
        return Redirect::route('especies.index')->with('delete', true);
    }

    public function backend()
    {
        return view('backend.index');
    }

    public function inactivos()
    {
        $inactivos = Noticia::onlyTrashed()->paginate();

        return view('backend.modules.especies.status.inactivos', compact('inactivos'));
    }

    public function activos($id)
    {
        Noticia::withTrashed()->find($id)->restore();

        return Redirect::route('especies.index');
    }
}
