

<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','AuthController@index');



Route::get('noticia', 'AuthController@vernoticia');

Route::get('registro', function () {
    return view('auth/registro');
});


Route::get('miasoguau', 'AuthController@miasoguauindex');

Route::get('noticiaAsored', function () {
    return view('auth/miasoguau/nuevaNoticiaAsored');
});

Route::get('donacion', function () {
    return view('auth/creardonacion');
});

Route::get('login', 'AuthController@showLogin'); // Mostrar login
Route::post('login', 'AuthController@postLogin'); // Verificar datos
Route::get('logout', 'AuthController@logOut'); // Finalizar sesión

Route::post('crearusuarios','AuthController@store');

Route::post('crearAsored','AuthController@crearAsored');
//Route::get('login', 'LoginController@showLogin');
//Route::post('login', 'AuthController@postLogin');

/* Rutas del Backend */

Route::get('backend', 'UsuarioController@backend');

/*
 * Estas rutas son rutas de tipos RESTFull cada una son rutas que tienen
 * los metodos necesarios para realizar el CRUD, traten de separar
 * cada rute Resource y sus metodos de elemento inactivo (si aplica)
 * y el metodo de retornar el elemento
 *
 * Se comentará cada ruta para tener un control del mismo
 * */

/*
 * Resource
 * Veterinarios Inactivos
 * Recovery de Veterinarios
 * */
Route::resource('vets', 'VeterinarioController');


/*
 * Resource
 * Usuarios Inactivos
 * Recovery de Usuarios
 * */

Route::resource('usuarios', 'UsuarioController');
Route::get('usuarios_inactivos', 'UsuarioController@inactivos');
Route::post('retorno/{id}', 'UsuarioController@activos'); //Usuarios



/*
*Resoruces
*Noticias Inactivas
*Recovery de Noticias
*/

Route::resource('noticiasbackend', 'NoticiaController');
Route::get('noticias_inactivos', 'NoticiaController@inactivos');
Route::post('retornonoticias/{id}', 'NoticiaController@activos'); //Usuarios


/*
*Resoruces
*Donaciones Inactivas
*Recovery de Donaciones
*/

Route::resource('donacionesbackend', 'DonacionController');
Route::get('donaciones_inactivos', 'DonacionController@inactivos');
Route::post('retornodonaciones/{id}', 'DonacionController@activos'); //Usuarios



/*
*Resoruces
*Donaciones Inactivas
*Recovery de Donaciones
*/

Route::resource('animales', 'AnimalController');
Route::get('animales_inactivos', 'AnimalController@inactivos');
Route::post('retornoanimales/{id}', 'AnimalController@activos'); //Usuarios


/*
*Resoruces
*Donaciones Inactivas
*Recovery de Donaciones
*/

Route::resource('especies', 'EspecieController');
Route::get('especies_inactivos', 'EspecieController@inactivos');
Route::post('retornoespecies/{id}', 'EspecieController@activos'); //Usuarios
