<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<title>Asoguau</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->

		<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/style.css')}}">
		<link rel="stylesheet" href="{{asset('css/main.css')}}">
		<link rel="stylesheet" href="{{asset('css/prueba.css')}}">
		<link rel="stylesheet" href="{{asset('css/main-responsive.css')}}">
		<link rel="stylesheet" href="{{asset('css/all.css')}}">
		<link rel="stylesheet" href="{{asset('css/perfect-scrollbar.css')}}">
		<link rel="stylesheet" href="{{asset('css/estilo.css')}}"  id="skin_color">
		<link rel="stylesheet" href="{{asset('css/bootstrap-fileupload.css')}}">
		<link rel="stylesheet" href="{{asset('css/social-buttons-3.css')}}">
		<!-- Optional theme -->
		<link rel="stylesheet" href="{{asset('css/bootstrap-theme.css')}}">



		<link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/creative.css')}}">
        <!-- end: MAIN CSS -->

     
		<!-- end: MAIN CSS -->
	</head>
	<!-- end: HEAD -->
	<body class="login example1">

		<div class="row" id="fondo_menu">
			
				<ul class="nav navbar-nav navbar">
					<li   role="presentation" class="active sombreado text-center">
						<a href="/">
							<h4 class="panel-default text-center ">
								<span class="glyphicon glyphicon-home"></span>
							</h4> 
							Inicio
						</a>
					</li>
					
                    <li class="sombreado" role="presentation">
                    	<a href="#">
                    		<h4   class="panel-default text-center ">
                    			<span class="glyphicon glyphicon-user"></span>
                    		</h4>
                    		&iquestQuienes somos?
                    	</a>
                    </li>
                    
					<li class="sombreado text-center" role="presentation">
                    	<a href="noticia">
                    		<h4   class="panel-default text-center ">
                    			<span class="glyphicon glyphicon-list-alt"></span>
                    		</h4>
                    		Noticias
                    	</a>
                    </li>
                    
					
					<li class="sombreado text-center" role="presentation">
						<a href="#">
							<h4 class="panel-default text-center"  >
								<span class="glyphicon glyphicon-earphone"></span>
							</h4>
							Contactanos
						</a>
					</li>

					@if (!Auth::guest() )
									<li  class="sombreado text-center" role="presentation" >
	                		<a href="miasoguau">
	                			<h4 class="panel-default text-center ">
	                				<span class="glyphicon glyphicon-cloud"></span>
	                			</h4>


	                				Mi Asoguau
	                			
	                		</a>
	                		</li>

					@endif

					@if (!Auth::guest() && Auth::user()->idtipousuario < 5)
							<li  class="sombreado text-center" role="presentation" >
	                		<a href="backend">
	                			<h4 class="panel-default text-center ">
	                				<span class="glyphicon glyphicon-cog"></span>
	                			</h4>


	                				Sistema
	                			
	                		</a>
	                		</li>
					@endif
                    
				</ul>
                
                
                 <ul class="nav navbar-nav navbar-right" style="margin-right: 4.1%;">
                	@if (Auth::guest())
					       <li  class="sombreado text-center" role="presentation" >
	                		<a href="login">
	                			<h4 class="panel-default text-center ">
	                				<span class="glyphicon glyphicon-log-in"></span>
	                			</h4>


	                				Ingresar
	                			
	                		</a>
	                		</li>
					    @else
			               		<li  class="sombreado text-center" role="presentation" >
			               		 <h2> {!! Auth::user()->nombre !!} {!! Auth::user()->apellido !!}</h2>
								</li>

								

								<li  class="sombreado text-center" role="presentation" >
	                		<a href="{{ action('AuthController@logOut') }}">
	                			<h4 class="panel-default text-center ">
	                				<span class="glyphicon glyphicon-new-window"></span>
	                			</h4>


	                				Logout
	                			
	                		</a>
	                		</li>

							
				        @endif
                 </ul>
</div>
		@yield('container')	
		
			<!-- end: COPYRIGHT -->
		
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<!--<![endif]-->
		<script src="{{asset('js/jquery-ui-1.10.2.custom.min.js')}}"></script>
		<script src="{{asset('js/bootstrap.js')}}"></script>
		<script src="{{asset('js/bootstrap-hover-dropdown.js')}}"></script>
		<script src="{{asset('js/jquery.blockUI.js')}}"></script>
		<script src="{{asset('js/jquery.icheck.js')}}"></script>
		<script src="{{asset('js/jquery.mousewheel.js')}}"></script>
		<script src="{{asset('js/perfect-scrollbar.js')}}"></script>
		<script src="{{asset('js/less-1.5.0.min.js')}}"></script>
		<script src="{{asset('js/jquery.cookie.js')}}"></script>
		<script src="{{asset('js/main.js')}}"></script>
		<script src="{{asset('js/jquery.validate.js')}}"></script>
		<script src="{{asset('js/login.js')}}"></script>
		<!-- jQuery -->
     <script src="{{asset('js/jquery.js')}}"></script>
     <script src="{{asset('js/bootstrap.min.js')}}"></script>
     <script src="{{asset('js/jquery.easing.min.js')}}"></script>
     <script src="{{asset('js/jquery.fittext.js')}}"></script>
     <script src="{{asset('js/wow.min.js')}}"></script>
     <script src="{{asset('js/creative.js')}}"></script>



     <script src="{{asset('js/bootstrap-fileupload.js')}}"></script>
		<script src="{{asset('js/jquery.pulsate/jquery.pulsate.js')}}"></script>
		<script src="{{asset('js/pages-user-profile.js')}}"></script>

		<!-- end: MAIN JAVASCRIPTS -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Login.init();
			});
		</script>
	</body>


	<!-- end: BODY -->
</html>