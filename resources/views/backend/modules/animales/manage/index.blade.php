@extends('backend.base')

@section('contenedor')
    <div id="page-wrapper" class="page-wrapper-cls">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">DASHBOARD \ <span style="color: blue;">Animal</span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Administraci&oacute;n de Animal</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Imagen de Perfil</h3>
                                    </div>
                                    <div class="panel-body">
                                        <img src="{{ asset('img/logo.png') }}" style="width: 250px; height: 250px;
                                        margin-left: 3%;"
                                             class="img-thumbnail"
                                             alt="Responsive
                                image">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-9">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Datos Basicos</h3>
                                    </div>
                                    <div class="panel-body">
                                            @if ($errors->any())
                        <div class="alert alert-danger">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Por favor corrige los siguentes errores:</strong>
                          <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                          </ul>
                        </div>
                      @endif

                                            @if(isset($edit))
                                                {!! Form::model($edit, ['route'=>['animales.update', $edit->id], 'method'=>'patch']) !!}
                                            @else
                                            {!! Form::open(array('method'=>'POST', 'route'=>'animales.store')) !!}
                                            @endif

                                            <div class="form-group col-lg-6">
                                                <i class="glyphicon glyphicon-text-color"></i>
                                                {!! Form::label('nombre', 'Nombre del Animal:') !!}
                                                {!! Form::text('nombre', Input::old('nombre'), array('class'=>'form-control')) !!}
                                            </div>

                                             <div class="form-group col-lg-6">
                                                <i class="fa fa-users"></i>
                                                 <div class="form-group col-lg-6">
                                                <i class="fa fa-users"></i>
                                                {!! Form::label('idespecie', 'Especie:') !!}
                                                {!! Form::select('idespecie', array('1'=>'Canino', '2'=>'Felini', '3'=>'Ave'), Input::old('idestatususuario'),
                                                ['class'=>'form-control']) !!}
                                            </div>

                                          </div>

                                            <div class="form-group col-lg-6">
                                                <i class="glyphicon glyphicon-text-color"></i>
                                                {!! Form::label('comentario', 'Comentario:') !!}
                                                {!! Form::textarea('comentario', Input::old('comentario'), array('class'=>'form-control')) !!}
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <i class="fa fa-users"></i>
                                                 <div class="form-group col-lg-6">
                                                <i class="fa fa-users"></i>
                                                {!! Form::label('idestatususuario', 'Estatus:') !!}
                                                {!! Form::select('idestatususuario', array('1'=>'Activo', '2'=>'Inactivo'), Input::old('idestatususuario'),
                                                ['class'=>'form-control']) !!}
                                            </div>
                                           
                                            </div>


                                              

                                            
                                    </div>
                                </div>
                            </div>

                            @if(isset($edit))
                                <div class="col-lg-12 text-center">
                                    <button type="submit" class="btn btn-success">Actualizar Usuario</button>
                                </div>
                            @else
                                <div class="col-lg-12 text-center">
                                    <button type="submit" class="btn btn-success">Registrar Usuario</button>
                                </div>
                            @endif
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                        @if(!$animales->isEmpty())
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre del Animal</th>
                                    <th>Especie</th>
                                    <th>Estatus</th>
                                    <th>Comentario</th>
                                    <th style="text-align: center">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($animales as $animal)
                                    <tr>
                                        <td>{{ $animal->id }}</td>
                                        <td>{{ $animal->nombre }}</td>
                                        <td>{{ $animal->especie->nombre }}</td>
                                        <td>{{ $animal->estatus->nombre }}</td>
                                        <td>{{ $animal->comentario }}</td>
                                        <td>
                                            {!! Form::open(array('method'=>'DELETE', 'route'=>['animales.destroy', $animal->id])) !!}

                                            {!! Form::submit('Suspender Animal', array('class'=>'btn btn-danger
                                            col-lg-12')) !!}

                                            {!! Form::close() !!}
                                        </td>
                                        <td>
                                            <a role="button" href="{{ URL::to("/animales/{$animal->id}/edit") }}"
                                               class="btn btn-info col-lg-12">Editar</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $animales->render() !!}
                        @else
                            No existe registro para mostrar
                        @endif
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
@endsection 