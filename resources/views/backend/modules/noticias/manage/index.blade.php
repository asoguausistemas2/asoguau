@extends('backend.base')

@section('contenedor')
    <div id="page-wrapper" class="page-wrapper-cls">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">DASHBOARD \ <span style="color: blue;">Noticias</span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Administraci&oacute;n de Noticias</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Imagen de Noticia</h3>
                                    </div>
                                    <div class="panel-body">
                                        <img src="{{ asset('img/logo.png') }}" style="width: 250px; height: 250px;
                                        margin-left: 3%;"
                                             class="img-thumbnail"
                                             alt="Responsive
                                image">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-offset-1 col-md-6">
                    
                <h1>AsoRed</h1>


                  @if ($errors->any())
                        <div class="alert alert-danger">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Por favor corrige los siguentes errores:</strong>
                          <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                          </ul>
                        </div>
                      @endif


                @if(isset($edit))
                            {!! Form::model($edit, ['route'=>['noticiasbackend.update', $edit->id], 'method'=>'patch']) !!}
                        @else
                        {!! Form::open(array('method'=>'POST', 'route'=>'noticiasbackend.store')) !!}
                        @endif
                    
                    <fieldset>
                        <div class="form-group">
                        {!! Form::label('titulo', 'Titulo de la Noticia:') !!}
                        {!! Form::text('titulo', Input::old('titulo'), array('class'=>'form-control')) !!}
                        </div>

                        <div class="form-group">
                            <span class="input-icon">
                             {!! Form::label('resumen', 'Resumen de Noticia:') !!}
                            {!! Form::textarea('resumen', Input::old('resuemn'), array('class'=>'form-control','size' => '30x5')) !!}

                       
                        </div>

                        <div class="form-group">
                            {!! Form::label('descripcion', 'Descripcion de la Noticia:') !!}
                            {!! Form::textarea('descripcion', Input::old('descripcion'), array('class'=>'form-control')) !!}
                        </div>
                
                            {!!Form::hidden('idtiponoticia',2,['class' =>'form-control','type' => 'hidden'])!!}
                            {!!Form::hidden('idusuario',Auth::user()->id,['class' =>'form-control','type' => 'hidden'])!!}
                        <div class="form-actions">

                         @if(isset($edit))
                                <div class="col-lg-12 text-center">
                                    <button type="submit" class="btn btn-success">Actualizar Noticia</button>
                                </div>
                            @else
                                <div class="col-lg-12 text-center">
                                    <button type="submit" class="btn btn-success">Registrar Noticia</button>
                                </div>
                            @endif
                            
                        </div>
                    </fieldset>
                {!! Form::close() !!}
            </div>

                       
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                        @if(!$noticias->isEmpty())
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Id Noticia</th>
                                    <th>Titulo Noticia</th>
                                    <th>Resumen Noticia</th>
                                    <th>Id Usuario</th>
                                    <th>Hora</th>
                                    <th style="text-align: center">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($noticias as $noticia)
                                    <tr>
                                        <td>{{ $noticia->id }}</td>
                                        <td>{{ $noticia->titulo }}</td>
                                        <td>{{ $noticia->resumen }}</td>
                                        <td>{{ $noticia->rol->nombre}}</td>
                                        <td>{{ $noticia->hora}}</td>
                                        <td>
                                            {!! Form::open(array('method'=>'DELETE', 'route'=>['noticiasbackend.destroy', $noticia->id])) !!}

                                            {!! Form::submit('Suspender Noticia', array('class'=>'btn btn-danger
                                            col-lg-12')) !!}

                                            {!! Form::close() !!}
                                        </td>
                                        <td>
                                            <a role="button" href="{{ URL::to("/noticiasbackend/{$noticia->id}/edit") }}"
                                               class="btn btn-info col-lg-12">Editar</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $noticias->render() !!}
                        @else
                            No existe registro para mostrar
                        @endif
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
@endsection