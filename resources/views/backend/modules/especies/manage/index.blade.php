@extends('backend.base')

@section('contenedor')
    <div id="page-wrapper" class="page-wrapper-cls">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">DASHBOARD \ <span style="color: blue;">Especie</span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Administraci&oacute;n de las Especies</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-3">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Imagen de Perfil</h3>
                                    </div>
                                    <div class="panel-body">
                                        <img src="{{ asset('img/logo.png') }}" style="width: 250px; height: 250px;
                                        margin-left: 3%;"
                                             class="img-thumbnail"
                                             alt="Responsive
                                image">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-9">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Datos Basicos</h3>
                                    </div>
                                    <div class="panel-body">
                                            @if ($errors->any())
                        <div class="alert alert-danger">
                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                          <strong>Por favor corrige los siguentes errores:</strong>
                          <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                          </ul>
                        </div>
                      @endif

                                            @if(isset($edit))
                                                {!! Form::model($edit, ['route'=>['especies.update', $edit->id], 'method'=>'patch']) !!}
                                            @else
                                            {!! Form::open(array('method'=>'POST', 'route'=>'especies.store')) !!}
                                            @endif

                                            <div class="form-group col-lg-6">
                                                <i class="glyphicon glyphicon-text-color"></i>
                                                {!! Form::label('nombre', 'Nueva Especie:') !!}
                                                {!! Form::text('nombre', Input::old('nombre'), array('class'=>'form-control')) !!}
                                            </div>

                                        
                                           

                                            


                                              

                                            
                                    </div>
                                </div>
                            </div>

                            @if(isset($edit))
                                <div class="col-lg-12 text-center">
                                    <button type="submit" class="btn btn-success">Actualizar Usuario</button>
                                </div>
                            @else
                                <div class="col-lg-12 text-center">
                                    <button type="submit" class="btn btn-success">Registrar Usuario</button>
                                </div>
                            @endif
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                        @if(!$especies->isEmpty())
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Especie</th>
                                    
                                    <th style="text-align: center">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($especies as $especie)
                                    <tr>
                                        <td>{{ $especie->id }}</td>
                                        <td>{{ $especie->nombre }}</td>
                                        
                                        <td>
                                            {!! Form::open(array('method'=>'DELETE', 'route'=>['especies.destroy', $especie->id])) !!}

                                            {!! Form::submit('Suspender Animal', array('class'=>'btn btn-danger
                                            col-lg-12')) !!}

                                            {!! Form::close() !!}
                                        </td>
                                        <td>
                                            <a role="button" href="{{ URL::to("/especies/{$especie->id}/edit") }}"
                                               class="btn btn-info col-lg-12">Editar</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $especies->render() !!}
                        @else
                            No existe registro para mostrar
                        @endif
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
@endsection 