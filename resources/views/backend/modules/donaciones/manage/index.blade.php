@extends('backend.base')

@section('contenedor')
    <div id="page-wrapper" class="page-wrapper-cls">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">DASHBOARD \ <span style="color: blue;">Donaciones</span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Administraci&oacute;n de Donaciones</h3>
                        </div>
                        <div class="panel-body">
                            

        <div class=" col-md-12">
                    

                     



            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                        @if(!$donaciones->isEmpty())
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Estatus</th>
                                    <th>Fecha</th>
                                    <th>Referencia</th>
                                    <th>Cantidad</th>
                                    <th style="text-align: center" class="col-md-3">Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($donaciones as $donacion)
                                    <tr>
                                        <td>{{ $donacion->id }}</td>
                                        <td>{{ $donacion->usuario->nombre }}</td>
                                        <td>{{ $donacion->estatus->nombre }}</td>
                                        <td>{{ $donacion->fecha}}</td>
                                        <td>{{ $donacion->referencia}}</td>
                                        <td>{{ $donacion->cantidad}}</td>
                                        <td>

                                            @if(isset($edit))
                                            {!! Form::model($edit, ['route'=>['donacionesbackend.update', $edit->id], 'method'=>'patch']) !!}

                                             <div class="form-group col-lg-6">
                                                
                                                
                                                {!! Form::hidden('cantidad', Input::old('cantidad'), array('class'=>'form-control')) !!}
                                            </div>

                                            <div class="form-group col-lg-6">
                                               
                                                
                                                {!! Form::hidden('referencia', Input::old('referencia'), array('class'=>'form-control')) !!}
                                            </div>

                                          
                                               
                                                {!! Form::hidden('fecha', Input::old('fecha'), array('class'=>'form-control')) !!}
                                            
                                          </div>

                                            <div class="form-group col-lg-12">
                                               
                                                {!! Form::label('idestatususuario', 'Estatus:') !!}
                                                {!! Form::select('idestatusdonacion', array('1'=>'Confirmada', '2'=>'Negada'), Input::old('idestatususuario'),
                                                ['class'=>'form-control']) !!}

                                               
                                            </div>


                                              <div class="form-group col-lg-6">
                                                
                                                {!! Form::hidden('idusuario', Input::old('idusuario'), array('class'=>'form-control')) !!}
                                            </div>


                                                {!! Form::submit('Enviar', array('class'=>'tn btn-info
                                            col-lg-15')) !!}
                                            {!!Form::close()!!}
                                            @else
                                             <a role="button" href="{{ URL::to("/donacionesbackend/{$donacion->id}/edit") }}"
                                               class="btn btn-info col-lg-12">Editar</a>
                                            @endif
                                        </td>
                                       
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $donaciones->render() !!}
                        @else
                            No existe registro para mostrar
                        @endif
                    </div>
                </div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
@endsection