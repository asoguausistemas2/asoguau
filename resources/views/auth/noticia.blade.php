 @extends('index')

@section('container')
                

 <h2 class="text-center">NOTICIAS ASOGUAU</h2>
	   <hr class="primary"></hr>
	    @foreach($noticias as $noticia)
                      @if ($noticia->idtiponoticia==1)
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/perdido.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded" >
                                    {{$noticia->titulo}}
                                </div>
                                <div class="project-name">
                                    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Leer mas</button>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
               
                @endif
        @endforeach 
                
                
               
            </div>
        </div>
		<!--FIN DE NOTICIAS ASOGUAU-->

        <h2 class="text-center">NOTICIAS ASORED</h2>
       <hr class="primary"></hr>
       
        <div class="container-fluid">
                <div class="row no-gutter">
                @foreach($noticias as $noticia)
                      @if ($noticia->idtiponoticia==2)
                
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="portfolio-box">
                        <img src="img/portfolio/perros5.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    {{$noticia->titulo}}
                                </div>
                                <div class="project-name">
                                   <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Leer mas</button>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
            </div>
        </div>

        @endif
        @endforeach
        <!--FIN DE NOTICIAS ASORED-->
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{$noticia->id}}</h4>
        </div>
        <div class="modal-body">
          <p>{{$noticia->descripcion}}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>


        @endsection