@extends('index')

@section('container')

<div class="col-md-offset-4 col-md-4">
					 @if ($errors->any())
					    <div class="alert alert-danger">
					      <button type="button" class="close" data-dismiss="alert">&times;</button>
					      <strong>Por favor corrige los siguentes errores:</strong>
					      <ul>
					      @foreach ($errors->all() as $error)
					        <li>{{ $error }}</li>
					      @endforeach
					      </ul>
					    </div>
					  @endif
					  	

				<h3>Registrarse</h3>


				{!!Form::open(['url' => 'crearusuarios']) !!}
					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> La informacion que ingreso no es la correcta.
					</div>
					<fieldset>
						<div class="form-group">
						{!!Form::text('nombre','',['class' =>'form-control','type' => 'text', 'placeholder' => 'Nombre'])!!}
							
						</div>
						<div class="form-group">
							{!!Form::text('apellido','',['class' =>'form-control','type' => 'text','placeholder' => 'Apellido'])!!}
						</div>
						<div class="form-group">
							<span class="input-icon">
							{!!Form::text('telefono','',['class' =>'form-control','type' => 'text','placeholder' => '04131234561'])!!}
							<i class="clip-phone-4"></i> </span>
						</div>
						<div class="form-group">
							<span class="input-icon">
							{!!Form::text('correo','',['class' =>'form-control','type' => 'email','placeholder' => 'Correo'])!!}
							<i class="fa fa-envelope"></i> </span>
						</div>
						
						<div class="form-group">
							<span class="input-icon">

								{!!Form::password('password',['class' =>'form-control','type' => 'password','placeholder' => 'Clave'])!!}
								<i class="fa fa-lock"></i> </span>
						</div>
						<div class="form-group">
							<span class="input-icon">
								{!!Form::password('password_confirmation',['class' =>'form-control','type' => 'password','placeholder' => 'Confirmar Clave'])!!}
								<i class="fa fa-lock"></i> </span>
						</div>
					
							{!!Form::hidden('idestatususuario','1',['class' =>'form-control','type' => 'hidden'])!!}
							{!!Form::hidden('idtipousuario','5',['class' =>'form-control','type' => 'hidden'])!!}
						<div class="form-actions">
							<a href="login">
								{!! Form::button('<i class="fa fa-arrow-circle-left"></i> Regresar ', array('type' => 'button', 'class' => 'btn btn-light-grey go-back')) !!}
							</a>

							{!! Form::button('Enviar <i class="fa fa-arrow-circle-right"></i>', array('type' => 'submit', 'class' => 'btn btn-bricky pull-right')) !!}   
							
							
						</div>
					</fieldset>
				{!! Form::close() !!}
			</div>
@endsection