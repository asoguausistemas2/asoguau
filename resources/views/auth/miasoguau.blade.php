	@extends('index')

@section('container')
	<!-- end: HEAD -->
	<div class="row">
			<div class="col-sm-5 col-md-4">
				<div class="user-left">
					<div class="center">
						
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<div class="user-image">
								<div class="fileupload-new thumbnail"><img src="{{ asset('img/user.png') }}" alt="">
								</div>
								<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
									<div class="user-image-buttons">
										<span class="btn btn-teal btn-file btn-sm"><span class="fileupload-new"><i class="fa fa-pencil"></i></span><span class="fileupload-exists"><i class="fa fa-pencil"></i></span>
											<input type="file">
										</span>
										<a href="#" class="btn fileupload-exists btn-bricky btn-sm" data-dismiss="fileupload">
											<i class="fa fa-times"></i>
										</a>
									</div>
								</div>
							</div>							
						</div>
						<table class="table table-condensed table-hover">
							<thead>
								<tr>
									<th colspan="3">Contact Information</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>url</td>
									<td>
										<a href="#">
											www.example.com
										</a></td>
										<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
								</tr>
								<tr>
									<td>email:</td>
									<td>
										<a href="">
											{!! Auth::user()->correo !!}
										</a></td>
									<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
								</tr>
								<tr>
									<td>phone:</td>
									<td>{!! Auth::user()->telefono !!}</td>
									<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
									</tr>
									<tr>
										<td>skye</td>
										<td>
										<a href="">
											peterclark82
										</a></td>
										<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
									</tr>
									<tr><td>Status</td>
										<td><span class="label label-sm label-info">Administrator</span></td>
										<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
									</tr>
							</tbody>
						</table>
						
						<table class="table table-condensed table-hover">
							<thead>
								<tr>
									<th colspan="3">Additional information</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Birth</td>
										<td>21 October 1982</td>
										<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
								</tr>
								<tr>
									<td>Groups</td>
									<td>New company web site development, HR Management</td>
									<td><a href="#panel_edit_account" class="show-tab"><i class="fa fa-pencil edit-user-info"></i></a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-sm-7 col-md-8">
					<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
						<li class="active">
							<a data-toggle="tab" href="#panel_overview">
								Mis Noticias
							</a>
						</li>
						<li class="">
							<a data-toggle="tab" href="#panel_edit_account">
								Crear Noticias
							</a>
						</li>
						<li>
							<a data-toggle="tab" href="#panel_projects">
								Donaciones
							</a>
						</li>
						</ul>

													<div class="tab-content">
														<!--PESTAÑA DE NOTICIAS-->
														<div id="panel_overview" class="tab-pane active">
															<div class="row">
			
																	<div class="col-sm-7 col-md-8">
																		<div class="panel panel-white">
													
																			<div class="panel-body panel-scroll ps-container" style="height:500px">
																				<ul class="activities">
																					@foreach($noticias as $noticia)
                      																	@if ($noticia->idusuario == Auth::user()->id)
																					<li>
																						<a class="activity" href="javascript:void(0)">
																							<i class="clip-upload-2 circle-icon circle-green"></i>
																							<span class="desc">{{$noticia->titulo}}</span>
																							<div class="time">
																								<i class="fa fa-time bigger-110"></i>
																								{{$noticia->hora}}
																							</div>
																						</a>
																					</li>
																					@endif
																					@endforeach
																					
																				</ul>
																			<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: -33px; width: 622px; display: none;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 36px; right: 3px; height: 300px; display: inherit;"><div class="ps-scrollbar-y" style="top: 24px; height: 206px;"></div></div></div>
																		</div>
																							
												
																	</div>
															</div>
														</div>
													
									<!--PESTAÑA CREAR NOTICIA-->
									<div id="panel_edit_account" class="tab-pane">
									 @if ($errors->any())
														    <div class="alert alert-danger">
														      <button type="button" class="close" data-dismiss="alert">&times;</button>
														      <strong>Por favor corrige los siguentes errores:</strong>
														      <ul>
														      @foreach ($errors->all() as $error)
														        <li>{{ $error }}</li>
														      @endforeach
														      </ul>
														    </div>
														  @endif
									{!!Form::open(['url' => 'crearAsored']) !!}
										
											<div class="row">
												<div class="col-md-offset-2 col-md-10">
													<div class="col-md-6">
												
													<div class="form-group">
														<label>
															Cargar Imagen
														</label>
														<div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
															<div class="fileupload-new thumbnail" style="width: 150px; height: 150px;"><img src="clip-one/assets/images/avatar-1-xl.jpg" alt="">
															</div>
															<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 650px; max-height: 350px; line-height: 10px;"></div>
															<div class="row">
															<div class="user-edit-image-buttons col-md-6">
																<span class="btn btn-bricky btn-file"><span class="fileupload-new"><i class="fa fa-picture"></i> Selecciona imagen</span><span class="fileupload-exists"><i class="fa fa-picture"></i> Cambiar</span>
																	<input type="file">
																</span>

																<a href="#" class="btn fileupload-exists btn-danger" data-dismiss="fileupload">
																	<i class="fa fa-times"></i> Quitar
																</a>
															</div>
															</div>
														</div>
													</div>
												</div>
												</div>
												<div class="col-md-6">
													

											
													<div class="form-group">
														<label class="control-label">
															Nombre de la Noticia:
														</label>
														{!!Form::text('titulo','',['class' =>'form-control','type' => 'text', 'placeholder' => 'Ttulo de la Noticia'])!!}
					
													</div>
													<div class="form-group">
														<label class="control-label">
															Resumen de la Noticia:
														</label>
														{!!Form::textarea('resumen','',['class' =>'form-control','type' => 'text','placeholder' => 'Resumen de Noticia' , 'size' => '30x5'])!!}
						
													</div>
													<div class="form-group">
														<label class="control-label">
															Descripcion de la Noticia:
														</label>
														{!!Form::textarea('descripcion','',['class' =>'form-control','type' => 'text','placeholder' => 'Descripcion'])!!}
								
													</div>
													{!!Form::hidden('idtiponoticia',2,['class' =>'form-control','type' => 'hidden'])!!}
													{!!Form::hidden('idusuario',Auth::user()->id,['class' =>'form-control','type' => 'hidden'])!!}
													{!!Form::hidden('hora','',['class' =>'form-control','type' => 'hidden'])!!}
						
					
												</div>
												
											</div>
											
											
											
											<div class="row">
												<div class="col-md-4">

													{!! Form::button('Publicar <i class="fa fa-arrow-circle-right"></i>', array('type' => 'submit', 'class' => 'btn btn-success btn-block')) !!}
													
												</div>
											</div>
										</form>
									</div>


									<!--PESTAÑA DONACIONES-->

									<div id="panel_projects" class="tab-pane">
										@if ($errors->any())
										    <div class="alert alert-danger">
										      <button type="button" class="close" data-dismiss="alert">&times;</button>
										      <strong>Por favor corrige los siguentes errores:</strong>
										      <ul>
										      @foreach ($errors->all() as $error)
										        <li>{{ $error }}</li>
										      @endforeach
										      </ul>
										    </div>
										  @endif
										<form action="#" role="form" id="form">
											<div class="row">
												{!!Form::open(['url' => 'donacionesbackend.store']) !!}
 
												<div class="col-md-8">
													<div class="form-group">
														<label class="control-label">
															Cantidad:
														</label>
															{!!Form::text('cantidad','',['class' =>'form-control','type' => 'text', 'placeholder' => '0,00 Bs'])!!}
							
														</div>
													<div class="form-group">
														<label class="control-label">
															Numero de Referencia:
														</label>
														{!!Form::text('referencia','',['class' =>'form-control','type' => 'text', 'placeholder' => 'Numero de Referencia o Deposito'])!!}
							
													</div>
													
													<div class="form-group">
														<label class="control-label">
															Banco Emisor:
														</label>
														{!!Form::text('banco','',['class' =>'form-control','type' => 'text'])!!}
							
													</div>
													{!!Form::hidden('idusuario',Auth::user()->id,['class' =>'form-control','type' => 'text'])!!}
													{!!Form::hidden('idestatusdonacion','3',['class' =>'form-control','type' => 'text'])!!}
													<div class="form-group">
													{!! Form::button('Enviar <i class="fa fa-arrow-circle-right"></i>', array('type' => 'submit', 'class' => 'btn btn-bricky pull-right')) !!}   
												

												</div>
												{!!Form::close()!!}
											</div>

											<div class="row">
												<div class="col-md-10">
													
													<div class="panel-body">
									<table class="table table-hover" id="sample-table-1">
										<thead>
											<tr>
												<th class="center">#</th>
												<th class="hidden-xs">Cantidad</th>
												<th>Banco</th>
												<th class="hidden-xs">Referencia</th>
												<th>Estatus</th>
												<th>Fecha</th>
											</tr>
										</thead>
										<tbody>
											@foreach($donaciones as $donacion)
												@if($donacion->idusuario == Auth::user()->id)
											<tr>
												<td class="center">{{$donacion->id}}</td>
												<td class="hidden-xs">{{$donacion->cantidad}}</td>
												<td>Google</td>
												<td>
													{{$donacion->referencia}}
												</td>
												<td>
													{{$donacion->estatus->nombre}}
												</td>
												<td class="hidden-xs">{{$donacion->created_at}}</td>
												<td class="center">
												<div class="visible-md visible-lg hidden-sm hidden-xs">

													{!! Form::open(array('method'=>'DELETE', 'route'=>['donacionesbackend.destroy', $donacion->id])) !!}

			                                            {!! Form::submit('x', array('class'=>'btn btn-danger col-lg-12')) !!}

                                            		{!! Form::close() !!}
												</div>
												</td>
											</tr>
												@endif
											@endforeach

										</tbody>
									</table>
								</div>

												</div>
											</div>
										</form>


									</div><!--END DE DONACIONES-->
								</div>




												</div><!--END PANEL BODY-->

										</div><!--END -->
										




		
		
	<!-- end: HEAD -->
	<!-- start: COPYRIGHT -->
			<div class="copyright">
				&copy; Fundaci&oacuten Asoguau, Powered by Acto Voluntario - Todos los derechos reservados.
			</div>

@endsection