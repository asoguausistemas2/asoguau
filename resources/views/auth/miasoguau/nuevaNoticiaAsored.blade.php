@extends('index')

@section('container')

<div class="col-md-offset-4 col-md-4">
					
				<h1>AsoRed</h1>


				  @if ($errors->any())
					    <div class="alert alert-danger">
					      <button type="button" class="close" data-dismiss="alert">&times;</button>
					      <strong>Por favor corrige los siguentes errores:</strong>
					      <ul>
					      @foreach ($errors->all() as $error)
					        <li>{{ $error }}</li>
					      @endforeach
					      </ul>
					    </div>
					  @endif


				{!!Form::open(['url' => 'crearAsored']) !!}
					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> La informacion que ingreso no es la correcta.
					</div>
					<fieldset>
						<div class="form-group">
						{!!Form::text('titulo','',['class' =>'form-control','type' => 'text', 'placeholder' => 'Ttulo de la Noticia'])!!}
							
						</div>

						<div class="form-group">
							<span class="input-icon">
							{!!Form::text('hora',Config::get('app.timezone'),['class' =>'form-control','type' => 'email','placeholder' => 'Hora de la Noticia'])!!}
							<i class="fa fa-envelope"></i> </span>
						</div>

						<div class="form-group">
							{!!Form::textarea('resumen','',['class' =>'form-control','type' => 'text','placeholder' => 'Resumen de Noticia' , 'size' => '30x5'])!!}
						</div>
						<div class="form-group">
							<span class="input-icon">
							{!!Form::textarea('descripcion','',['class' =>'form-control','type' => 'text','placeholder' => 'Descripcion'])!!}
							
						</div>
				
							{!!Form::hidden('idtiponoticia',2,['class' =>'form-control','type' => 'hidden'])!!}
							{!!Form::hidden('idusuario',Auth::user()->id,['class' =>'form-control','type' => 'hidden'])!!}
						<div class="form-actions">
							

							{!! Form::button('Enviar <i class="fa fa-arrow-circle-right"></i>', array('type' => 'submit', 'class' => 'btn btn-bricky pull-right')) !!}   
							
							
						</div>
					</fieldset>
				{!! Form::close() !!}
			</div>
@endsection