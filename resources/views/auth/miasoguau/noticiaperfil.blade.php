<div id="panel_overview" class="tab-pane active">
															<div class="row">
			
																	<div class="col-sm-7 col-md-8">
																		<div class="panel panel-white">
													
																			<div class="panel-body panel-scroll ps-container" style="height:500px">
																				<ul class="activities">
																					@foreach($noticias as $noticia)
                      																	@if ($noticia->idusuario == Auth::user()->id)
																					<li>
																						<a class="activity" href="javascript:void(0)">
																							<i class="clip-upload-2 circle-icon circle-green"></i>
																							<span class="desc">{{$noticia->titulo}}</span>
																							<div class="time">
																								<i class="fa fa-time bigger-110"></i>
																								{{$noticia->hora}}
																							</div>
																						</a>
																					</li>
																					@endif
																					@endforeach
																					
																				</ul>
																			<div class="ps-scrollbar-x-rail" style="left: 0px; bottom: -33px; width: 622px; display: none;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 36px; right: 3px; height: 300px; display: inherit;"><div class="ps-scrollbar-y" style="top: 24px; height: 206px;"></div></div></div>
																		</div>
																							
												
																	</div>
															</div>
														</div>