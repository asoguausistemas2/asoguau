@extends('index')

@section('container')

<div class="col-md-offset-4 col-md-4">
					
				<h1>AsoRed</h1>


				  @if ($errors->any())
					    <div class="alert alert-danger">
					      <button type="button" class="close" data-dismiss="alert">&times;</button>
					      <strong>Por favor corrige los siguentes errores:</strong>
					      <ul>
					      @foreach ($errors->all() as $error)
					        <li>{{ $error }}</li>
					      @endforeach
					      </ul>
					    </div>
					  @endif


				{!!Form::open(['url' => 'crearAsored']) !!}
					<div class="errorHandler alert alert-danger no-display">
						<i class="fa fa-remove-sign"></i> La informacion que ingreso no es la correcta.
					</div>
					<fieldset>
						<div class="form-group">
							{!!Form::text('cantidad','',['class' =>'form-control','type' => 'text', 'placeholder' => '0,00 Bs'])!!}
							
						</div>
						
						<div class="form-group">
							{!!Form::text('referencia','',['class' =>'form-control','type' => 'text', 'placeholder' => 'N Referencia/N Deposito'])!!}
							
						</div>

						{!!Form::text('idusuario',Auth::user()->id,['class' =>'form-control','type' => 'hidden'])!!}
						{!!Form::text('idestatusdonacion',3,['class' =>'form-control','type' => 'hidden'])!!}
						
				
						
							

							{!! Form::button('Enviar <i class="fa fa-arrow-circle-right"></i>', array('type' => 'submit', 'class' => 'btn btn-bricky pull-right')) !!}   
							
							
						</div>
					</fieldset>
				{!! Form::close() !!}
			</div>
@endsection				