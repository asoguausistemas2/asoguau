@extends('index')

@section('container')
				

		<div class="row" id="bloque2">
			<div class="col-md-4">
			
			<div class="row" id="redes-sociales">
				<img src="img/logo666.png" class="img-responsive" style="width:50%; height:50%; margin-left:28%;">
				<ul id= "iconos">
					<li id="facebook"><a href="https://es-la.facebook.com/ASOGUAU" title="Síguenos en Facebook"></a></li>
					<li id="twitter"><a href="https://twitter.com/asoguau" title="Síguenos en Twitter"></a> </li>
					<li id="contacto"><a href="https://instagram.com/asoguau" title="Siguenos en Instagram"></a></li>
				</ul>
				
				</div>
			</div>
			<!--carrusel-->
			<div class="col-md-8" id="carrusel">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
					<div class="item active">
					  <img src="img/perros.gif" alt="perros1">
					  <div class="carousel-caption">
						...
					  </div>
					</div>
					<div class="item">
					  <img src="img/perros.gif" alt="perros2">
					  <div class="carousel-caption">
						...
					  </div>
					</div>
					...
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Antes</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Despues</span>
				  </a>
				</div>
			</div>
		</div>
         
      </div>
	  
		  
	<!--Comienzo de NOTICIAS-->
				<div class="row" id="noticias">
					<div class="col-md-6">
					  <div class="panel-group" role="tablist">
						<div class="panel panel-default">
						  <div class="panel-heading " role="tab" id="collapseListGroupHeading1">
							<h4 class="panel-title">
							  <a class="" role="button" data-toggle="collapse" href="#collapseListGroup1" aria-expanded="true" aria-controls="collapseListGroup1">
								NOTICIA ASOGUAU
							  </a>
							</h4>
						  </div>
						  <div style="" aria-expanded="false" id="collapseListGroup1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading1">
							<ul class="list-group">
							  <li class="list-group-item">NOTICIA 1</li>
							  <li class="list-group-item">One itmus ac facilin</li>
							  <li class="list-group-item">Second eros</li>
							  <li class="list-group-item">Second eros</li>
							</ul>
							
						  </div>
						</div>
					  </div>
					</div>
				
							 
		
		<!--FIN DE NOTICIAS ASOGUAU-->
	
	<!--COMIENZO DE NOTICIAS ASORED-->
	<div class="col-md-6">
	 <div class="panel-group" role="tablist">
						<div class="panel panel-default">
						  <div class="panel-heading" role="tab" id="collapseListGroupHeading2">
							<h4 class="panel-title">
							  <a class="" role="button" data-toggle="collapse" href="#collapseListGroup2" aria-expanded="true" aria-controls="collapseListGroup2">
								Noticia Asored
							  </a>
							</h4>
						  </div>
						  <div style="" aria-expanded="false" id="collapseListGroup2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collapseListGroupHeading2">
							<ul class="list-group">
							  <li class="list-group-item">NOTICIA 1</li>
							  <li class="list-group-item">One itmus ac facilin</li>
							  <li class="list-group-item">Second eros</li>
							  <li class="list-group-item">Second eros</li>
							</ul>
							
						  </div>
						</div>
					  </div>
				
			</div>
			</div>
		
		
	<!-- end: HEAD -->
	<!-- start: COPYRIGHT -->
			<div class="copyright">
				&copy; Fundaci&oacuten Asoguau, Powered by Acto Voluntario - Todos los derechos reservados.
			</div>
@endsection